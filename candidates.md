# Interesting Software to Look Into

Contrary to the actual awesome list in the [README](README.md), which is a list of programs I have personally used and deemed "awesome", this is a list programs that are interesting for one reason or another, but require further exploration before I choose to use them or not. This list was created primarily to deal with the fact that there was a long list of programs I wanted to look into, and I did not want to lose them - furthermore, this allows frequent visitors to my awesome list to find new programs that they may want to explore without my recommendation.

As such, this software may come with a significantly longer description outlining what I _think_ I know about it and it is significantly less meticilously categorized.

# Software

- [AB Download Manager](https://github.com/amir1376/ab-download-manager) - Download manager for Windows & Linux
- [age](https://yaeba.github.io/blog/age/) - Encryption tool
- [Barrier](https://github.com/debauchee/barrier) - Software KVM software forked from Synergy
- [Bruno](https://www.usebruno.com/) - a tool for testing web APIs that works well with `git`.
- [Checkmate](https://github.com/bluewave-labs/checkmate) - Uptime monitoring system
- [Clash](https://birkhofflee.github.io/clash/) - cross-platform, rules-based proxy
- [Cobalt](https://github.com/imputnet/cobalt) - Media Downloader
- [connet](https://github.com/connet-dev/connet) - An extremely interesting development, a p2p reverse-proxy.
- [context_menu](https://github.com/saleguas/context_menu) - Set up complex context menu actions for Windows/Linux with Python
- [crypt.fyi](https://www.crypt.fyi) - Self-hostable, secure pastebin service
- [Dioxus](https://github.com/DioxusLabs/dioxus) - A Rust framework for Web, GUI, and Mobile applications.
- [dtrx](https://github.com/dtrx-py/dtrx) - Archive Extraction Tool
- [Feh](https://www.ghacks.net/2019/09/16/feh-is-a-light-weight-command-line-image-viewer-for-linux/) - lightweight commandline image-viewer
- [frp](https://github.com/fatedier/frp) - A reverse proxy TCP/UDP protocol servers
- [fzf-help](https://github.com/BartSte/fzf-help) - An fzf plugin that works with `--help` output of applications to assist with discoverability and shell completion
- [Garage](https://garagehq.deuxfleurs.fr/) - Self-hostable distributed storage service
- [git-crypt](https://www.agwa.name/projects/git-crypt/) - System for storing secrets in `git` using GPG.
- [gpg-tui](https://github.com/orhun/gpg-tui) - GPG Key Management TUI
- [GoRSS](https://github.com/Lallassu/gorss) - NCurses-based RSS Feed Reader
- [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway) - Turn GRPC APIs into JSON/HTTP APIs
- [hishtory](https://github.com/ddworken/hishtory) - Synchronized, deduplicated, and queriable shell history system
- [ingestr](https://bruin-data.github.io/ingestr/getting-started/quickstart.html) - A system for transferring data between databases, can be thought of as "rsync for databases" but definitely more involved
- [jless](https://jless.io/) - TUI for viewing JSON-formatted information
- [KryptEY](https://github.com/amnesica/KryptEY) - A keyboard that allows E2EE communication through any medium.
- [lapdev](https://github.com/lapce/lapdev) - Self-hosted remote development enviroment management system
- [legit](https://git.icyphox.sh/legit) - Git Web Front-end written in Go
- [Lynis](https://github.com/CISOfy/lynis) - Security scanning and audit tool
- [maxun](https://github.com/getmaxun/maxun): No code, web-data extraction and scraping system
- [mdbook](https://rust-lang.github.io/mdBook/guide/creating.html) - A system for generating "book-style" tutorial websites from Markdown
- [meli](https://jvns.ca/blog/2017/12/23/segfault-debugging/) - Terminal email client
- [meilisearch](https://www.meilisearch.com/) - Extremely fast search written in Rust
- [nap](https://github.com/maaslalani/nap) - TUI snippet manager
- [nebula](https://github.com/slackhq/nebula) - A mesh VPN system ([more info here](https://arstechnica.com/gadgets/2019/12/how-to-set-up-your-own-nebula-mesh-vpn-step-by-step/))
- [Neon](https://github.com/neondatabase/neon?tab=readme-ov-file): Serverless PostgreSQL
- [NixOS](https://nix.dev/) - A Linux OS with a focus on reproduceable, configuration-managed environments
- [notekit](https://github.com/blackhole89/notekit/) - Note taking application with tablet and markdown support
- [Oryx](https://github.com/pythops/oryx): A TUI for packet sniffing and network debugging, written in Rust.
- [ouch](https://github.com/ouch-org/ouch): CLI Compression tool with wide format support
- [picosnitch](https://github.com/elesiuta/picosnitch) - A per-application network monitoring service
- [Posting](https://github.com/darrenburns/posting) - TUI HTTP Client (similar to Postman)
- [Process Compose](https://github.com/F1bonacc1/process-compose): This is an interesting program that I found while searching for a scheduling system for periodic tasks (like an advanced cron with a GUI). The presence of the word "scheduler" in the description of the software intrigued me, however, it doesn't appear to really have scheduling capability as such. Instead it appears to be some kind of process manager with an interface similar to Docker Compose, but without the actual Docker piece.
- [punktf](https://github.com/Shemnei/punktf): Multi-target, multi-profile dotfile manager
- [Quickemu](https://github.com/quickemu-project/quickemu) - An ease-of-use layer on top of `qemu` that makes spinning up VMs easy.
- [rainfrog](https://github.com/achristmascarl/rainfrog) - Management TUI for PostgreSQL
- [recipes](https://github.com/vabene1111/recipes) - Cooking recipe management system
- [RecoverPy](https://github.com/PabloLec/RecoverPy) - Advanced disk/file recovery utility
- [RQlite](https://github.com/rqlite/rqlite) - Distributed database based on SQLite
- [SeaweedFS](https://github.com/chrislusf/seaweedfs) - Distributed file system
- [shareboxx](https://github.com/dividebysandwich/shareboxx) - Turns a small portable computer (like a Raspberry Pi) into a portable server for local filesharing without an internet connection
- [ssh-chat](https://github.com/shazow/ssh-chat) - A shat service that operates over SSH, easy to deploy on an ad-hoc basis
- [StatiCrypt](https://github.com/robinmoisson/staticrypt): An interesting way of sending information around by creating an encrypted HTML file which can then be opened and decrypted by any browser (assuming they have the password). This allows the sending of encrypted files without having to worry about system/client/software compatibility as one would have to if they were using any other means of encrypting information.
  - Inherently very similar to [portable-secret](https://mprimi.github.io/portable-secret/) which was further developed into: https://www.privacyprotect.dev/ 
- [Stackstorm](https://docs.stackstorm.com/overview.html) - Distributed integration & automation platform
- [supavisor](https://github.com/supabase/supavisor) - PostgreSQL connection-pooling
- [tinyfilemanager](https://github.com/prasathmani/tinyfilemanager) - in-browser file management application
- [Trailblaze](https://github.com/trailbaseio/trailbase) - App server built on Rust & SQLite
- Turnkey Operating Systems & Similar: Systems designed for managing self-hosted applications easily as packaged "apps".
  - [CasaOS](https://casaos.io/): Installed on top of Debian, used to manage entire server, can install applications through "App Stores" which are themselves git repositories.
  - [Yunohost](https://yunohost.org/)
  - [RunTipi](https://runtipi.io/)
  - [Umbrel](https://umbrel.com/)
  - [CapRover](https://github.com/caprover/caprover): More of a platform for WebApp hosting and development, but can be layered on top of most OSs.
  - [1Panel](https://1panel.hk/): Originally designed as a Server Management Interface, this system can also install certain common platforms such as web servers and databases.
  - [Sandstorm](https://sandstorm.org/)
- [TruffleHog](https://github.com/trufflesecurity/trufflehog) - Scans for leaked secrets
- [Unciv](https://github.com/yairm210/Unciv) - Open source Android/Windows/Mac/Linux remake of Civ5
- [Waydroid](https://waydro.id/) - An Android Container System for running Android apps on Linux
- [Xip.io](http://xip.io/) - Wildcard DNS
- [xpra](https://pypi.org/project/xpra/) - A python project that allows applications to run remotely but with the window-management features of the local machine
- [zellij](https://github.com/zellij-org/zellij) - A tiling TUI "window manager" similar to `tmux`, written in Rust.

## Libraries

- [PgQueuer](https://github.com/janbjorge/PgQueuer):  PgQueuer is a Python library leveraging PostgreSQL for efficient job queuing. This feels like it would be a very effective tool to queue up jobs in a distributed environment where RabbitMQ or Kafka aren't quite good enough because the jobs take much longer and require things like status/progress updates.
- [pg_replicate](https://github.com/supabase/pg_replicate): This is a Rust library for building applications that replicate information between databases.

# Instructions, Tutorials, and Articles

- [Conditional Use of Jump Hosts in SSH Configurations](https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Proxies_and_Jump_Hosts#Conditional_Use_of_Jump_Hosts): Seems like it would be valuable to explore when trying to deal with reliability issues in SSH jump hosts.

# Services

This section covers services which are hosted by a third party (and may or may not have a corresponding self-hostable version).

- [Bitwarden Secrets Manager](https://bitwarden.com/help/secrets-manager-cli/#tab-inline-7x4v8ktNB7trBxUx9cFsab) - Secrets manager for server networks
- [Cloaked Wireless](https://cloakedwireless.com/new-plans/) - A secure SIM card plan provider
- [NetBird](https://netbird.io/) - Managed Wireguard solution, similar to Tailscale, but can be both self-hosted and used as a service
- [Notesnook](https://github.com/streetwriters/notesnook) - E2EE Note-taking service & associated apps
- [PrivTracker](https://privtracker.com/) - On-demand private torrent trackers for sharing files with groups of friends