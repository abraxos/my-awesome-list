# my-awesome-list

Eugene's list of awesome software, technical articles, and instructions.

Please note that the programs in this list are considered "awesome" by me personally. It means I have used these programs, like them, and recommend them for a given purpose. There is also a separate list of [candidates](candidates.md) which contains all the programs I want to look into, but have not yet had the chance.





## Software



### CLI Applications

- [autojump](https://github.com/wting/autojump) - A cd command that learns from your filesystem browsing
- [bandwhich](https://github.com/imsnif/bandwhich) - Connection/network traffic monitoring program
- [bat](https://github.com/sharkdp/bat) - A better cat with syntax highlighting
- [borg](https://www.borgbackup.org/) - A deduplicating, encrypted backup system
- [bottom](https://github.com/ClementTsang/bottom) - An alternative to top
- [ByteStash](https://github.com/jordan-dalby/ByteStash) - WebApp for code Snippets
- [cookiecutter](https://github.com/cookiecutter/cookiecutter) - A program for generating templated software project directories
  - [Documentation](https://cookiecutter.readthedocs.io/en/latest/) - Includes pointers to [useful examples](https://github.com/audreyr/cookiecutter-pypackage).
  - [My Cookiecutter Templates](https://gitlab.com/abraxos/cookiecutter-templates)
- [croc](https://github.com/schollz/croc) - An application for sending files securely between two machines, almost identical to [magic-wormhole](https://magic-wormhole.readthedocs.io/en/latest/welcome.html), but with the key benefit of being interruptible.
- [cryfs](https://github.com/cryfs/cryfs) - Encrypted file system in user-space with Windows, Linux, and MacOS support
- [delta](https://github.com/dandavison/delta) - A better diff with highlighting (written in Rust)
- [diskonaut](https://github.com/imsnif/diskonaut) - Disk space explorer TUI written in Rust
- [dive](https://github.com/wagoodman/dive) -  A tool for exploring each layer in a docker image 
- [Ducker](https://github.com/robertpsoane/ducker) - TUI docker manager, similar to [dry](https://github.com/moncho/dry) but I find it easier to use.
- [duf](https://github.com/muesli/duf) - Rust-based `df` replacement
- [emacs](https://www.gnu.org/software/emacs/) - Terminal Text Editor
  - [ivy](https://github.com/abo-abo/swiper) - Auto-completion system for `emacs`.
  - [projectile](https://docs.projectile.mx/projectile/usage.html) - Fuzzy search for `emacs`.
  - [visual-regexp](https://github.com/benma/visual-regexp.el) - Improved regexp find-replace for `emacs`.
- [fd-find](https://github.com/sharkdp/fd) - Rust-based `find` replacement
- [ffsend](https://github.com/timvisee/ffsend) - commandline [Send](https://send.vis.ee/) client
- [fswatch](https://github.com/emcrisostomo/fswatch)([doc](https://emcrisostomo.github.io/fswatch/doc/)) - Monitor directories for changes and execute programs in response
- [The Hacker's Choice](https://github.com/hackerschoice) - A repository of projects, demos, hacking tools, tips and tricks by an IT Security Research group of the same name. Includes several interesting and potentially useful tools.
  - [SSH Key Backdoor](https://github.com/hackerschoice/ssh-key-backdoor) - Exploits users who copy SSH keys without checking the contents to create backdoors to other systems.
- [Hackingtool](https://github.com/Z4nzu/hackingtool) - A collection of hacking tools in one convenient CLI package
- [harlequin](https://github.com/tconbeer/harlequin) - Advanced TUI IDE(?) for working with relational databases such as postgreSQL and SQLite.
- [lazydocker](https://github.com/jesseduffield/lazydocker) - TUI for docker management (similar to `ducker`)
- [libgen-cli](https://github.com/ciehanski/libgen-cli) - CLI for downloading books from LibGen
- [magic-wormhole](https://magic-wormhole.readthedocs.io/en/latest/welcome.html) - An application for sending files securely between machines
  - [wormhole-gui](https://github.com/Jacalz/wormhole-gui) - A GUI for magic-wormhole, written in Go
- [mermaid.js](https://mermaid.js.org/intro/) - A language for creating diagrams
- [matrix-commander](https://github.com/8go/matrix-commander) - A CLI (*not* TUI) application for interacting with Matrix, can be very useful for automated sending of messages etc
- [navi](https://github.com/denisidoro/navi) - Interactive command-line cheatsheet system (I use this ALL the fime)
- [netscanner](https://github.com/Chleba/netscanner) -  Network scanner & diagnostic tool with modern TUI 
- [no-more-secrets](https://github.com/bartobri/no-more-secrets) - A commandline application that simulates the visual decryption effect from the 1992 movie Sneakers
- [OCRmyPDF](https://github.com/ocrmypdf/OCRmyPDF) - CLI for adding OCR to any PDF files
- [pass](https://www.passwordstore.org/) - CLI password manager with many compatible GUI applications for various OSs
- [passgen](https://gitlab.com/passgen/passgen-rs) - Password generator with interesting ability to use AI to generate words that don't exist, but are easy to type (integrates well with [pass](https://www.passwordstore.org/))
- [pipx](https://github.com/pypa/pipx) - Python package manager which installs each package in its own virtualenv
- [pre-commit](https://pre-commit.com/) - A framework for managing and maintaining multi-language pre-commit hooks.
- [QbittorrentUI](https://github.com/rmartin16/qbittorrentui) - TUI for QBittorrent management
- [resume.md](https://github.com/mikepqr/resume.md) - A nice tool to generate a good-looking resume using markdown
- [ripgrep](https://github.com/BurntSushi/ripgrep) - A grep replacement written in rust
- [shiori](https://github.com/go-shiori/shiori) - A self-hosted bookmarks manager (similar to Pocket)
- [starship](https://starship.rs/) - A cross-shell prompt written in rust
- [tlock](https://github.com/eklairs/tlock) - Terminal Two-Factor Auth Token Manager
- [toolong](https://github.com/Textualize/toolong) - TUI for working with logs including advanced features such as JSON log analysis. Replacement for combinations of software such as `less`, `jq`, and `grep`.
- [tmuxp](https://github.com/tmux-python/tmuxp) - tmux session manager
- [watchexec](https://github.com/watchexec/watchexec) - File-system event reactor aimed at developers
- [WinFSP](https://github.com/billziss-gh/winfsp) - Essentially, FUSE for Windows, works very well with [sshfs-win](https://github.com/billziss-gh/sshfs-win)



### Self-hostable Services

- [Bitmagnet](https://bitmagnet.io/) - Self-hosted torrent indexer using the DHT
- [BitWarden](https://help.bitwarden.com/article/install-on-premise/#install-bitwarden) - web-based password manager with [desktop, browser, CLI, and mobile apps](https://bitwarden.com/#download).
- [Blocky](https://github.com/0xERR0R/blocky) - Easy to configure and manage ad-blocking DNS server with advanced features like conditional DNS
- [Calibre-Web](https://github.com/janeczku/calibre-web) - ebook management web-app
- [Cockpit](https://cockpit-project.org/) - Server management Web Interface
- [Dagu](https://dagu.readthedocs.io/en/latest/) - Easy-to configure cronjob management system with advanced features focused on expressing jobs as directed acyclic graphs (DAGs)
- [dokku](https://dokku.com/) - Self-hosted PaaS which I use as the primary environment for my own micro-services
- [dufs](https://github.com/sigoden/dufs) - File server with a WebUI that supports search, access control, downloads, uploads, and WebDAV - inherently similar to [miniserve](https://github.com/svenstaro/miniserve) but with a bigger feature set at comparable complexity
- [DNS-over-HTTPS-over-Tor](https://github.com/piskyscan/dns_over_tls_over_tor) - a way to secure and anonymize DNS traffic
- [dockge](https://github.com/louislam/dockge) - A convenient UI for managing docker containers and stacks
- [drawio](https://github.com/jgraph/drawio) - A web app for creating diagrams
- [Gitea](https://about.gitea.com/) - A self-hosted git server similar to Github or Gitlab
- [Gotify](https://gotify.net/) - Self-hosted simple notification system
- [Homepage](https://gethomepage.dev/latest/) - Simplistic, self-hostable web-page with bookmarks, links to services, widgets, and elementary monitoring - great for a homelab setup to track services in one place.
- [Invidious](https://invidious.io/) - A self-hosted alternative front-end for YouTube
- [Immich](https://immich.app/) - "Information Silo" for photos with very advanced search functionality and ML features, but confined to your server rather than someone else's. Has excellent [mobile](https://play.google.com/store/apps/details?id=app.alextran.immich) [apps](https://apps.apple.com/us/app/immich/id1613945652) to make photo/video upload easy.
- [Jackett](https://github.com/Jackett/Jackett) - API Support for torrent trackers
- [jellyfin](https://jellyfin.org/) - Media server
- [LiteShort](https://git.ikl.sh/132ikl/liteshort) - URL link shortener
- [mdWiki](https://github.com/Dynalon/mdwiki?tab=readme-ov-file) - A markdown wiki system deployable in a single HTML file
- [MicroBin](https://github.com/szabodanika/microbin) - A self-hostable file sharing and URL-shortening service written in Rust
- [Miniflux](https://github.com/miniflux/v2) - Self-hostable RSS Feed Reader with excellent integration support
- [Overseerr](https://overseerr.dev/) - Request management and media discovery
- [paperless-ngx](https://docs.paperless-ngx.com/) - Document management, hosting, tagging, and indexing server
- [Pi-hole](https://pi-hole.net/) - DNS server to block advertising and other unwanted domains
  - [Blocklist Collection](https://firebog.net/) - Someone curates a list of blocklists and categorizes them
  - [Pi-hole/Unbound/Wireguard](https://github.com/notasausage/pi-hole-unbound-wireguard) - A custom, ad-blocking VPN setup
- [Portainer](https://www.portainer.io/) - A WebUI for managing Docker containers
- [Proxmox](https://www.proxmox.com/en/): A hypervisor operating system with support for both VMs and LXC containers. This is essentially what my whole network runs on now.
  - [proxmove](https://github.com/ossobv/proxmove) - A program for moving VMs between [Proxmox](https://www.proxmox.com/en/) clusters.
  - [Proxmox VE Helper-Scripts](https://tteck.github.io/Proxmox/)
- [qBittorrent](https://www.qbittorrent.org/) - My preferred bittorrent application (usable as GUI application, or as a web-service)
- [Scrutiny](https://github.com/AnalogJ/scrutiny) - HDD Monitoring System
- [Seafile](https://www.seafile.com/en/home/) - Web-based file-sync service, like Dropbox
  - [Seafile-cli](https://help.seafile.com/en/syncing_client/linux-cli.html) - A command-line client for seafile
  - [Seafile Docker](https://github.com/haiwen/seafile-docker) - Docker deployment for Seafile
- [SFTPGo](https://github.com/drakkan/sftpgo) - SFTP (and other file) server with management interface
- [sist2](https://github.com/simon987/sist2) - A self-hostable file-system indexer with a WebUI that allows efficient search through large collections of files
- [ssh-chat](https://github.com/shazow/ssh-chat) - An extremely easy-to-deploy chat system that can be set up for on-demand communication
- [sqlite-web](https://github.com/coleifer/sqlite-web) - Not really a "Self-hosted" thing, more of a CLI tool that runs a webUI for working with SQLite databases, none
- [StashApp](https://stashapp.cc/) - Self hosted "silo" for NSFW content
- [Synapse](https://matrix.org/docs/projects/server/synapse) - Server software for the Synapse/Matrix messaging system
  - [Element](https://element.io/) - A messaging client
  - [Matrix](https://matrix.org/) - Federated messaging system
- [Syncthing](https://syncthing.net/) - A distributed file-synchronization system
  - [Syncthing Discovery](https://docs.syncthing.net/users/stdiscosrv.html) - A self-hostable discovery service for Syncthing
- [Temporal](https://docs.temporal.io/self-hosted-guide/setup) - A distributed computing system for scheduled and resilient/fault-tolerant execution or long-running tasks
- [Toolong](https://github.com/Textualize/toolong) - TUI for less & tail-like functionality for analyzing large files
- [Trilium](https://github.com/zadam/trilium) - Note taking and knowledge management webapp
- [TubeArchivist](https://github.com/tubearchivist/tubearchivist) - YouTube media server
- [Warpgate](https://github.com/warp-tech/warpgate) - SSH Bastion
- [WebMin](https://webmin.com/) - WebUI for system administration
- [Wiki.JS](https://wiki.js.org/) - Self-hosted markdown wiki with a nice UI
- [Wireguard](https://www.wireguard.com/) - a new kind of VPN
  - [Wireguard on PirateDocs](https://github.com/pirate/wireguard-docs)
  - [Wireguard on ArchWiki](https://wiki.archlinux.org/index.php/WireGuard)
  - [Wireguard over WSTunnel](https://kirill888.github.io/notes/wireguard-via-websocket/) - A means to obfuscate Wireguard traffic for bypassing restrictions
    - [WSTunnel](https://github.com/erebe/wstunnel) - A tool for tunnelling traffic over the WebSocket (HTTP) protocol witten in Haskell
    - [vTunnel](https://github.com/net-byte/vTunnel) - A WebSocket compatible Android application
    - [Wireguard/WebSocket Tunnelling on Linux](https://nerdonthestreet.com/wiki?find=Set+Up+a+WireGuard+VPN+Server+with+WebSocket+Tunneling)

### GUI Applications

- [Bypass Paywalls (clean)](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean) - A firefox extension for bypassing paywalls
- [cromite](https://github.com/uazo/cromite) - Privacy-focused browser, fork of Bromite, which is itself a fork of Chromium for Android
- [DrawIO Desktop](https://github.com/jgraph/drawio-desktop) - Electron build of Draw.IO diagramming software
- [Heroic Games Launcher](https://heroicgameslauncher.com/) - Excellent for playing Windows games on Linux, very easy to set up and use
- [MarkText](https://github.com/marktext/marktext) - Markdown text editor
- [Neat Download Manager](https://github.com/Neatdownloadmanager/Neat-Download-Manager/releases) - A download manager with media grabbing (via extension)
- [Obsidian](https://obsidian.md/) - Note taking application with markdown, tagging, and graph support
- [ripme](https://github.com/RipMeApp/ripme/blob/master/README.md) - a tool for downloading images from various webpages
- [Rufus](https://rufus.ie/en/) - A USB imaging tool for writing various OS images to USB with interesting customization options (windows only)
- [Rymdport (formerly wormhole-gui)](https://github.com/Jacalz/rymdport) - A GUI for magic-wormhole, written in Go
- [SingleFile](https://github.com/gildas-lormeau/SingleFile) - A web extension for downloading entire webpages as a single HTML file. Extremely useful for Archiving.
- [SiriKali](https://mhogomchungu.github.io/sirikali/) - A GUI tool for working with goCryptFS and similar encrypted file systems (works on Mac, Windows, and Linux)
- [sshfs-win](https://github.com/billziss-gh/sshfs-win) - Allows access to SSHFS shares as Windows Drives
- [xpipe](https://github.com/xpipe-io/xpipe) - Infrastructure management system (SSH, Docker-integration, etc)
- [zed](https://zed.dev/) - Lightweight & performant IDE and code-editor.

#### Android
- [GitSync](https://github.com/ViscousPot/GitSync) - Syncs information between your Android device and a git repository.
- [OSS DocumentScanner](https://github.com/Akylas/OSS-DocumentScanner) - Camera-based document scanner

#### MacOS

- [AltTab](https://alt-tab-macos.netlify.app/) - Changes Cmd-Tab behavior on MacOS to more resemble Windows & Linux (and has other options)
- [Clocker](https://abhishekbanthia.com/clocker/) - Menu Bar application that can help keep track of multiple timezones as well as integrating with your calendar to let you know about upcoming events ([Source Code](https://github.com/n0shake/Clocker))
- [Homebrew](https://brew.sh/) - Package Manager for OS X
- [Ice](https://github.com/jordanbaird/Ice) - Menu Bar manager for MacOS which can help with controlling items, especially if you have a Macbook with a "notch".
- [Maccy](https://maccy.app/) - Clipboard Manager, really helps with productivity and something I use all the time, just make sure you configure it not to save your clipboard history to disk ([Source Code](https://github.com/p0deje/Maccy))
- [Rectangle](https://rectangleapp.com/) - Window tiling application for MacOS with lots of useful shortcuts.

#### Fonts

- [Departure Mono](https://departuremono.com/): monospaced pixel font inspired by the constraints of early command-line and graphical user interfaces, the tiny pixel fonts of the late 90s/early 00s, and sci-fi concepts from film and television.



### Libraries & Development Tools

- [C](https://www.iso.org/standard/74528.html)
  - [microui](https://github.com/rxi/microui) - A tiny, portable, immediate-mode UI library written in ANSI C
- [Go (Golang)]()
  - [awesome-go](https://github.com/avelino/awesome-go)
  - [cli](https://github.com/urfave/cli) - system for generating CLIs
  - [cobra](https://github.com/spf13/cobra) - system for generating CLIs
  - [gin](https://github.com/gin-gonic/gin) - HTTP web framework
  - [kit](https://github.com/go-kit/kit) - collection of utilities for microservices
  - [List of recommended Go libraries](https://threedots.tech/post/list-of-recommended-libraries/)
  - [lo](https://github.com/samber/lo) - functional programming library with generics support
  - [logrus](https://github.com/sirupsen/logrus) - structured logging
  - [testify](https://github.com/stretchr/testify) - effective testing utilities
  - [viper](https://github.com/spf13/viper) - configuration library
- [Publicly Available APIs](https://github.com/public-apis/public-apis) - A list of public APIs for various applications
- [Python](https://www.python.org)
  - Development Tools:
    - [black](https://black.readthedocs.io/en/stable/index.html) - Code formatter for Python.
    - [blinker](https://blinker.readthedocs.io/en/stable/) - Simple object-to-object and broadcast signaling for Python.
    - [pyright](https://github.com/microsoft/pyright) - Static type checker for Python (typically plugged into IDE).
    - [ruff](https://docs.astral.sh/ruff/) - An extremely fast Python linter and code formatter, written in Rust.
    - [uv](https://docs.astral.sh/uv/) - An extremely fast Python package and project manager, written in Rust.
  - Libraries:
    - [apprise](https://github.com/caronc/apprise) - A CLI program and Python Library for sending notifications through virtually any popular communication system
    - [Avro](https://avro.apache.org/docs/1.11.1/getting-started-python/) - defined-schema compressed binary object data and RPC protocol as mentioned in [Designing Data-Intensive Applications](https://www.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/)
    - [Beartype](https://beartype.readthedocs.io/en/latest/) - A runtime typing and validation library
    - [Blessed](https://github.com/jquast/blessed) - A library for creating TUIs
    - [Celery](https://github.com/celery/celery) - Queue-based task management system
      - [Flower](https://github.com/mher/flower) - Web-based management UI for Celery queues
    - [Click](https://click.palletsprojects.com/en/7.x/) - A commandline arguments library
    - [CustomTkinter](https://github.com/TomSchimansky/CustomTkinter) - Python GUI library
    - [DearPyGUI](https://github.com/hoffstadt/DearPyGui) - A real-time GUI system/framework for Python
    - [diagrams](https://github.com/mingrammer/diagrams) - A library designed for generating diagrams described with code (e.g. diagrams-as-code)
    - [DiskCache](https://github.com/grantjenks/python-diskcache) - A disk-backed highly-performant cache for Python
    - [environs](https://pypi.org/project/environs/) - A python library for effectively handling environment variables
    - [Fabric](https://www.fabfile.org/) - Remote management via SSH
    - [FastAPI](https://github.com/tiangolo/fastapi) - High performance web framework for Python
    - [funcy](https://github.com/Suor/funcy) - functional programming utilties for Python
    - [inotify_simple](https://github.com/chrisjbillington/inotify_simple) - An easy-to-use iNotify library for python
    - [logzero](https://logzero.readthedocs.io/en/latest/index.html?) - Python logging library
    - [Loguru](https://github.com/Delgan/loguru) - alternative logging system for Python
    - [minotaur](https://github.com/giannitedesco/minotaur) - iNotify library for async programming
    - [Pipe](https://github.com/JulienPalard/Pipe) - Library for efficient and clean functional syntax in Python
    - [Plotly/Dash](https://github.com/plotly/dash) - A Python Library for Complex Web Dashboard
    - [Privy](https://pypi.org/project/privy/#usage) - Utilities for encryption and secrets management
    - [Py_CUI](https://github.com/jwlodek/py_cui) - Python TUI library
    - [PyInfra](https://docs.pyinfra.com/en/2.x/api/deploys.html) - Essentially a re-implmentation of Ansible, this system is interesting because it can be called directly from a python script and thus used as a library when necessary.
    - [PyQT](https://wiki.python.org/moin/PyQt) - Python GUI library
      - [PyQT Tutorial](https://build-system.fman.io/pyqt5-tutorial)
    - [Pyrage](https://github.com/woodruffw/pyrage) - Python library written in Rust for interacting with the `age` encryption tool
    - [Pyrogram](https://github.com/pyrogram/pyrogram) - Async telegram bot and application Python library
    - [Pyroscope](https://github.com/pyroscope-io/pyroscope) - Performance analytics for python programs
    - [pyrsistent](https://github.com/tobgu/pyrsistent) - Persistent/Immutable/Functional data structures for Python
    - [PyTest-Postgresql](https://pypi.org/project/pytest-postgresql/) - A pytest plugin for unit tests against postgres databases
    - [python-fuse](https://github.com/libfuse/python-fuse) - A library for writing custom FUSE-based file systems ([tutorial](https://github.com/libfuse/python-fuse/wiki) available)
    - [python-telegram-bot](https://python-telegram-bot.readthedocs.io/en/stable/) - Appears to be the best-documented and most actively developed library for Python Telegram bots
    - [Rich](https://github.com/willmcgugan/rich) - A library for adding color and styling to terminal applications
      - [Textual](https://github.com/willmcgugan/textual) - A set of Widgets for rich
    - [Rocketry](https://github.com/Miksus/rocketry) - Scheduling library
    - [shortuuid](https://pypi.org/project/shortuuid/) - Library for more compact, URL-safe, UUIDs
    - [sqlitedict](https://github.com/piskvorky/sqlitedict) -  Persistent dict, backed by sqlite3 and pickle, multithread-safe
    - [TerminalTextEffects (TTE)](https://chrisbuilds.github.io/terminaltexteffects/) - A library of interesting terminal TUI effects for loading and startup animations - can also be used as a CLI program
    - [Typeguard](https://pypi.org/project/typeguard/) - A runtime type-checking library
    - [Typer](https://typer.tiangolo.com/) - A library for type-checked commandline arguments based on [click](https://click.palletsprojects.com/en/7.x/)
    - [WatchDog](https://github.com/gorakhargosh/watchdog) - file-system event monitoring
    - [wrapt](https://github.com/GrahamDumpleton/wrapt) - A Python module for decorators, wrappers and monkey patching
    - [ZeroRPC](https://github.com/0rpc/zerorpc-python) - An RPC system for Python based on ZeroMQ with an effective IPC mechanism
- [Sense HAT](https://pythonhosted.org/sense-hat/)
  - [Sense HAT Temperature Correction](https://github.com/initialstate/wunderground-sensehat/wiki/Part-3.-Sense-HAT-Temperature-Correction)
  - [zerorpc](https://github.com/0rpc/zerorpc-python) - A library for extremely fast, simple RPC calls using ZeroMQ and MessagePack




## Services & Website Tools

This section covers services which are hosted by a third party (and may or may not have a corresponding self-hostable version).

- [4d2.org](https://4d2.org/) A collection of lovable crazy people operating self-hostable services for the public benefit
- [Ente Auth](https://ente.io/auth) E2EE, Cloud-based 2FA System (works on Linux, Windows, MacOS, Android and iOS)
- [envs.net](https://envs.net/) Very similar to https://4d2.org (mentioned above) with a selection of self-hosted services available to the public, most notably, [search](https://searx.envs.net/) which I use as my default search engine.
- [Library of Leaks](https://search.libraryofleaks.org/) A search engine for leaked information hosted by [DDOSecrets](https://ddosecrets.com/).
- [pico.sh](https://pico.sh/): The next evolution of my unhinged obsession with SSH, this is a series of cloud systems built on top of SSH, including things like static sites, a blogging platform, tunnels, pastes, and a docker registry.
  - [pipe.pico.sh](https://pipe.pico.sh/): Authenticated Pub/Sub Queues over SSH
- [ZFS/RAIDZ Capacity Calculator](https://wintelguy.com/zfs-calc.pl)



## Other Awesome Lists

- [Best of...](https://github.com/best-of-lists/best-of)
  - [Best of Python](https://github.com/ml-tooling/best-of-python)
- [Deploy Your Own SaaS](https://github.com/Atarity/deploy-your-own-saas/blob/master/README.md)
- [Go Recipes](https://github.com/nikolaydubina/go-recipes)
- [Open Source iOS Apps](https://github.com/dkhamsing/open-source-ios-apps)
- [Project Awesome](https://project-awesome.org/)
  - [A list of new(ish) commandline tools](https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/)
  - [Awesome Android Apps](https://github.com/Psyhackological/AAA)
  - [Awesome CLI Apps](https://github.com/herrbischoff/awesome-command-line-apps)
  - [Awesome Command Line (CLI/TUI) Programs](https://github.com/toolleeo/awesome-cli-apps-in-a-csv)
  - [Awesome Console Services](https://github.com/chubin/awesome-console-services)
  - [Awesome Cursor Rules](https://github.com/PatrickJS/awesome-cursorrules)
  - [Awesome Datahoarding](https://github.com/simon987/awesome-datahoarding)
  - [Awesome Falsehood](https://github.com/kdeldycke/awesome-falsehood) - List of Commonly-believed Falsehoods
  - [Awesome Go](https://github.com/avelino/awesome-go)
  - [Awesome Hacker Search Tools](https://github.com/edoardottt/awesome-hacker-search-engines)
  - [Awesome iOS Applications (Open-Source)](https://github.com/dkhamsing/open-source-ios-apps)
  - [Awesome Mac](https://github.com/jaywcjlove/awesome-mac)
  - [Awesome Matrix](https://github.com/jryans/awesome-matrix)
  - [Awesome Piracy](https://github.com/Igglybuff/awesome-piracy) - Excellent tools regardless of whether you're a pirate or not
  - [Awesome Python](https://github.com/vinta/awesome-python) [(and another, less organized awesome-python list)](https://github.com/uhub/awesome-python)
    - [Awesome Python Typing](https://github.com/typeddjango/awesome-python-typing) - tools for type-checking in Python
  - [Awesome Rust](https://github.com/BurntSushi/awesome-rust), [and another one](https://github.com/rust-unofficial/awesome-rust)
    - [Awesome Rust TUI (Ratatui) Applications](https://github.com/ratatui-org/awesome-ratatui?tab=readme-ov-file)
  - [Awesome Self-hosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
    - [awesome-selfhosted.net](https://awesome-selfhosted.net/index.html#) - An indexed website version of the above
  - [Awesome SSH](https://github.com/moul/awesome-ssh/blob/master/README.md)
  - [Awesome SysAdmin](https://github.com/n1trux/awesome-sysadmin)
  - [Awesome TUIs](https://github.com/rothgar/awesome-tuis)
  - [Awesomo](https://github.com/lk-geimfari/awesomo) - Another awesome list by language
  - [CLI/TUI Awesome List of Programs](https://github.com/toolleeo/cli-apps)
- [Public APIs](https://github.com/public-apis/public-apis)




## Instructions, Tutorials, and Articles

- [Converting Java App into Linux Applications](https://github.com/maynooth/CS210/wiki/Convert-Java-Executable-to-Linux-Executable)
- [Blocklist Collection for Pihole](https://firebog.net/)
- [Build a Tiny Certificate Authority For Your Homelab](https://smallstep.com/blog/build-a-tiny-ca-with-raspberry-pi-yubikey/)
- [Building Interactive SSH Programs](https://drewdevault.com/2019/09/02/Interactive-SSH-programs.html)
- [CDN Up-and-Running](https://github.com/leandromoreira/cdn-up-and-running)
- [Debian Passwordless Disk Encryption](https://wejn.org/how-to-make-passwordless-cryptsetup.html)
- [How to run a DNS server](https://cr.yp.to/djbdns/run-server.html)
- [Missing Semester of your CS Education](https://missing.csail.mit.edu/2020/potpourri/)
- [Mutt - How-to](https://www.ghacks.net/2019/11/23/mutt-is-a-command-line-email-app-for-linux-and-heres-how-to-set-it-up/)
- [Perfect Media Server](https://perfectmediaserver.com/) - A listing of common, useful technologies for self-hosted servers
- [PostgreSQL](https://www.postgresql.org/)
  - [PostgreSQL How-tos](https://gitlab.com/postgres-ai/postgresql-consulting/postgres-howtos)
  - [PostgreSQL is Enough](https://gist.github.com/cpursley/c8fb81fe8a7e5df038158bdfe0f06dbb)
- [Raspberry Pi](https://www.raspberrypi.org/)
  - [Cluster Hat](https://www.raspberrypi.org/magpi/clusterhat-review-cluster-hat-kit/)
  - [Google Vision Kit](https://aiyprojects.withgoogle.com/vision/#project-overview)
  - [Google Voice Kit](https://aiyprojects.withgoogle.com/voice#project-overview)
  - [Pi-Hole](https://pi-hole.net/):
    - [Building a PiHole for Privacy and Performance](https://thesmashy.medium.com/building-a-pihole-for-privacy-and-performance-f762dbcb66e5)
    - [Pi-Hole, Wireguard, and Privoxy](https://github.com/crozuk/pi-hole-wireguard-privoxy)
  - [Raspberry Pi Cluster](https://www.softwareteamlead.com/raspberry-pi-cluster/)
  - [Raspbian OS](https://www.raspberrypi.org/downloads/raspbian/)
  - [How to control power outlets with a Raspberry Pi](https://www.samkear.com/hardware/control-power-outlets-wirelessly-raspberry-pi)
  - [Undocumented Non-Interactive Mode in Raspi-Config](https://loganmarchione.com/2021/07/raspi-configs-mostly-undocumented-non-interactive-mode/)
- [Roll Your Own Network](https://roll.urown.net/)
- [Rust](https://doc.rust-lang.org/book/)
  - [Debugging a Segfault in Rust](https://jvns.ca/blog/2017/12/23/segfault-debugging/)
- [Sacred Heart Self-hosted](https://github.com/sacredheartsc/selfhosted) - Guide and source code for a self-hosted network and service-set managed through ansible
- [Tailscale is Pretty Useful](https://blog.6nok.org/tailscale-is-pretty-useful/)
- [TerminalApps](https://terminal-apps.dev/) - List of TUI applications
- [Warez: The Infrastructure and Aesthetics of Piracy](https://github.com/gfscott/warez-the-infrastructure-and-aesthetics-of-piracy) - A book about internet piracy that is also published open source on GitHub
- [Wireless Power Outlets](https://timleland.com/wireless-power-outlets/)
